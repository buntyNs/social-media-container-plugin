<?php

namespace SocialSharePlugin\Providers;

use IO\Helper\TemplateContainer;
use IO\Helper\ResourceContainer;
use Plenty\Plugin\Events\Dispatcher;
use Plenty\Plugin\ServiceProvider;
use Plenty\Plugin\Templates\Twig;



class PlaceholderServiceProvider extends ServiceProvider
{

	/**
	 * Register the service provider.
	 */
	public function register() 
	{

	}

	public function boot(Twig $twig, Dispatcher $eventDispatcher)
    {
        $eventDispatcher->listen('IO.Resources.Import', function (ResourceContainer $container)
        {
            $container->addScriptTemplate('SocialSharePlugin::content.Scripts');
        }, 0);
    }
}